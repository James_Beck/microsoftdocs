﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocsTutorial.Models;
using Microsoft.EntityFrameworkCore;

namespace DocsTutorial.Models
{
    public class ToDoContext : DbContext
    {
        public ToDoContext(DbContextOptions<ToDoContext> options) : base(options)
        { }

        public DbSet<ToDoItem> ToDoItems { get; set; }
    }
}
